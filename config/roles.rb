# frozen_string_literal: true

require './config/takeoff'

class Roles
  def self.short_output(roles)
    if roles.respond_to?(:each)
      roles.map do |role|
        shorten_role = role.split('-').last

        shorten_role == 'node' ? 'deploy-node' : shorten_role
      end.join(', ')
    else
      roles
    end
  end

  def initialize(environment)
    @roles = Takeoff.env[environment]
  end

  def regular_roles
    service_roles.values.flatten.uniq
  end

  def sidekiq_roles
    [service_roles['sidekiq'], service_roles['sidekiq-cluster']].flatten.compact
  end

  def gitaly_roles
    service_roles['gitaly']&.first
  end

  def regular_no_gitaly
    regular_roles - [gitaly_roles].flatten
  end

  def regular_and_blessed
    regular_roles + [blessed_node]
  end

  def services_for_role(role, skip: [])
    service_roles.each_with_object([]) do |(k, v), services|
      services << k if v.include?(role) && !skip.include?(k)
    end
  end

  private

  def method_missing(name, *args, &block)
    @roles.key?(name.to_s) ? @roles[name.to_s] : super
  end

  def respond_to_missing?(name, _include_private = false)
    @roles.key?(name)
  end
end
