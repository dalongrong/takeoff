# frozen_string_literal: true

Dir['./lib/post_checks/*.rb'].each { |file| require file }

module PostChecks
  def self.[](post_check)
    const_get("PostChecks::#{post_check.split('_').map(&:capitalize).join}")
  end
end
