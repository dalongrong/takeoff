# frozen_string_literal: true

require './lib/steps/base'

module Steps
  class BaseRoles < Base
    attr_reader :roles
    private :roles

    attr_accessor :spinners

    def initialize(roles, options = {})
      @roles = roles

      super(options)
    end

    private

    def run_command_on_roles(*args, &block)
      CommandRunner.run_command_on_roles(*args, &block)
    end

    def run_command_concurrently(*args, &block)
      CommandRunner.run_command_concurrently(*args, &block)
    end
  end
end
