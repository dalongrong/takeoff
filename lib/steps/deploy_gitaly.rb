# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class DeployGitaly < BaseRoles
    def run
      return unless roles_to_deploy

      gitaly_step = Steps::DeployRestart.new(roles,
                                             roles_to_deploy: roles.gitaly_roles,
                                             version: version,
                                             command_wrapper: options[:command_wrapper],
                                             gitaly_restart: version_changed?)

      gitaly_step.run!

      @error = gitaly_step.error
    end

    private

    def version_change
      @version_change ||= VersionChange.new(roles.gitaly_roles, roles.blessed_node, 'GITALY_SERVER_VERSION')
    end

    def version_changed?
      @version_changed ||= version_change.changed?
    end

    def roles_to_deploy
      @roles_to_deploy ||= roles.gitaly_roles
    end

    def version
      @options[:version]
    end
  end
end
