# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class Migrations < BaseRoles
    def run
      return unless run_migrations?

      run_command_on_roles(roles.blessed_node,
                           migrate_command,
                           title: title,
                           silence_stdout: false) do |stdout_output|
        migration_analyzer = Migration::Analyzer.new(stdout_output, type: type)

        if migration_analyzer.error
          logger.error(migration_analyzer.error)
          slack&.migration_error(type,
                                migration_analyzer.total_time,
                                migration_analyzer.error)
        end

        if migration_analyzer.any?
          logger.info(migration_analyzer.as_table)
          slack&.migrations(type, migration_analyzer.total_time)
        end
      end
    end

    protected

    def migrate_command
      'sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate'
    end

    def run_migrations?
      roles.run_migrations
    end

    def title
      'Running first batch of migrations from the blessed node'
    end

    def type
      'migrations'
    end
  end
end
