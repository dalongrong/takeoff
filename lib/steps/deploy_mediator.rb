# frozen_string_literal: true

require './lib/steps/base_roles'
require './lib/steps/deploy_package'
require './lib/steps/unicorn_hup'
require './lib/steps/services_restart'

module Steps
  class DeployMediator < BaseRoles
    def run
      Thread.abort_on_exception = true

      run_steps!

      abort_on_error
    end

    private

    def run_steps!
      concurrent_steps.map do |step|
        Thread.new do
          Thread.current.name = "#{Thread.current.object_id}:#{Steps.step_name(step)}"

          step.run!
        end
      end.each(&:join)
    end

    def abort_on_error
      step_with_error = concurrent_steps.find { |step| step.error }

      raise ScriptError, step_with_error.error if step_with_error
    end

    def concurrent_steps
      @concurrent_steps ||=
        begin
          roles.regular_no_gitaly.each_with_object([]) do |role, steps|
            next if resumer.has_substep?(role)

            steps << Steps::DeployRestart.new(roles, steps_options.merge(roles_to_deploy: role))
          end
        end
    end

    def steps_options
      {
        command_wrapper: options[:command_wrapper],
        version: version,
        spinners: spinners,
        resumer: resumer,
        pages_restart: pages_version_changed?,
        workhorse_restart: workhorse_version_changed?,
        'post_checks' => ['retry_on_failure', 'register_success']
      }
    end

    def resumer
      options[:resumer]
    end

    def version
      @options[:version]
    end

    def spinners
      @spinners ||= TTY::Spinner::Multi.new(*spinner_options)
    end

    def spinner_options
      [
        ":spinner \e[1mDeploying to the rest of the nodes\e[0m",
        format: :dots,
        error_mark: ANSI::RED % '✖',
        success_mark: "\e[1m\e[32m✓\e[0m\e[0m"
      ]
    end

    def pages_version_changed?
      @pages_version_change ||= VersionChange.new(roles.regular_roles.last, roles.blessed_node, 'GITLAB_PAGES_VERSION').changed?
    end

    def workhorse_version_changed?
      @workhorse_version_change ||= VersionChange.new(roles.regular_roles.last, roles.blessed_node, 'GITLAB_WORKHORSE_VERSION').changed?
    end
  end
end
