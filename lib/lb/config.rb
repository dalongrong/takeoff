# frozen_string_literal: true
module Lb
  class Config
    ROLE_TO_LB = {
      web: %w[fe],
      pages: %w[fe-pages],
      api: %w[fe],
      git: %w[fe fe-altssh],
      registry: %w[fe-registry]
    }

    def initialize(environment)
      @environment = environment
    end

    def lb_list(role: nil)
      lbs = role ? find_lbs(role) : all_lbs

      lbs.join(':')
    end

    def has_lb?(role)
      lb_for_role(role)
    end

    private

    def find_lbs(role)
      all_lbs.find_all do |lb|
        regex_list = lb_for_role(role).map do |lb_string|
          Regexp.new("#{lb_string}-\\d")
        end

        lb =~ Regexp.union(regex_list)
      end
    end

    def lb_for_role(role)
      role_key = role.split('-').last

      ROLE_TO_LB[role_key.to_sym]
    end

    def all_lbs
      @lb_list ||=
        JSON.parse(knife_result)['rows'].map do |row|
          row.keys.first
        end
    end

    def knife_result
      `#{knife_command}`
    end

    def knife_command
      "bundle exec knife search -i 'roles:" + base_role + "' -a fqdn -Fjson"
    end

    def base_role
      "#{@environment}-base-lb"
    end
  end
end
