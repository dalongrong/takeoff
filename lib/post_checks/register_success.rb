# frozen_string_literal: true

module PostChecks
  class RegisterSuccess
    def initialize(error, step:)
      @error = error
      @step = step
    end

    def run
      return if @error

      @step.resumer.add_substep(@step.role)
    end
  end
end
