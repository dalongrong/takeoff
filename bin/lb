#!/usr/bin/env ruby
# frozen_string_literal: true

# Example usage: lb -e production -r web -x <download/stats/drain/maint/ready>
#
require 'json'
require 'optparse'
require './config/roles'
require './config/takeoff'

DEFAULT_ENV = 'gstg'

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: version [options]'

      opts.on('-e', "--env=#{DEFAULT_ENV}", "Environment to use. Defaults to #{DEFAULT_ENV}.") do |env|
        args[:env] = env
      end

      opts.on('-r', '--role=api', 'Specify the role. Defaults to all.') do |role|
        args[:role] = role
      end

      opts.on('-a', '--action=drain', 'Specify the action. <drain/maint/ready>') do |action|
        args[:action] = action
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts

        exit
      end
    end

    opt_parser.parse!(options)
    args
  end
end

options = Parser.parse(ARGV)

env = options[:env] || DEFAULT_ENV

require './lib/command_runner'
require './lib/progress_runner'
require './lib/lb/config'
require './lib/lb/command_wrapper'

roles = Roles.new(env)
regular_roles = roles.regular_roles
found_roles = options[:role] ? regular_roles.find { |r| r.end_with?(options[:role]) } : regular_roles
wrapper = Lb::CommandWrapper.new(env)

CommandRunner.run_command_on_roles(found_roles,
                                   wrapper.public_send(options[:action], role: found_roles),
                                   title: "Running #{options[:action]}")
