# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/post_migrations'

describe Steps::PostMigrations do
  before { enable_dry_run }

  context 'with post-deployment migrations' do
    subject { described_class.new(Roles.new('gstg'), post_deployment: true) }

    it 'outputs the right command' do
      command = <<~COMMAND
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end

  context 'without post-deployment migrations' do
    subject { described_class.new(Roles.new('gstg'), post_deployment: true) }

    it 'outputs the right command' do
      allow(subject).to receive(:run_post_deploy_migrations?).and_return(false)
      expect { subject.run }.to output('').to_stdout_from_any_process
    end
  end
end
