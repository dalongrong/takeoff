# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/track_deployment'

describe Steps::TrackDeployment do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('gstg')) }

  it 'outputs the right command' do
    command = <<~COMMAND.strip.tr("\n", ' ')
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake gitlab:track_deployment
    COMMAND

    expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
  end
end
