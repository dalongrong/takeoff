# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/deploy_page'

describe Steps::DeployPage do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('gstg')) }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl deploy-page down
    COMMAND

    expect { subject.run }.to output(command).to_stdout_from_any_process
  end
end
