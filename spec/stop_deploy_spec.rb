# frozen_string_literal: true

require 'spec_helper'
require './lib/deploy_status'
require './config/roles'

describe StopDeploy do
  let(:pid) { 99999 }

  before do
    stub_const('StopDeploy::WAIT_SECONDS', 0.1)
  end

  it 'kills the process with SIGINT' do
    allow(File).to receive(:exist?).and_return(false)
    allow(File).to receive(:read).and_return(pid)

    expect(Process).to receive(:kill).with('SIGINT', pid)
    expect(described_class).to receive(:alive?).and_return(false)
    expect(Process).not_to receive(:kill).with('SIGKILL', pid)

    described_class.stop
  end

  it 'kills the process with SIGKILL' do
    allow(File).to receive(:exist?).and_return(false)
    allow(File).to receive(:read).and_return(pid)

    expect(Process).to receive(:kill).with('SIGINT', pid)
    expect(described_class).to receive(:alive?).and_return(true).exactly(described_class::WAIT_ATTEMPTS).times
    expect(Process).to receive(:kill).with('SIGKILL', pid)

    described_class.stop
  end
end
