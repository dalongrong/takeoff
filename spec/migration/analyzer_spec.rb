require 'spec_helper'
require './lib/migration/analyzer'

describe Migration::Analyzer do
  subject { described_class.new(migration_output, type: 'test') }

  context 'successful migration' do
    let(:migration_output) { File.read('./spec/fixtures/migration/migration.log') }

    it 'reports the total time' do
      expect(subject.total_time).to eq('14 minutes')
    end

    it 'returns true if there is data' do
      expect(subject.any?).to be true
    end

    it 'returns an ASCII table' do
      table = <<~TABLE.rstrip

        +-------------------------------------------+----------+
        |test ( > 60 seconds)                       |Duration  |
        +-------------------------------------------+----------+
        |AddIndexForTrackingMirroredCiCdRepositories|6 minutes |
        |AddTmpStagePriorityIndexToCiBuilds         |5 minutes |
        |Total (all timings)                        |14 minutes|
        +-------------------------------------------+----------+
      TABLE

      expect(subject.as_table).to eq(table)
    end
  end

  context 'failed migration' do
    let(:migration_output) { File.read('./spec/fixtures/migration/migration.error.log') }

    it 'reports the total time' do
      expect(subject.total_time).to eq('16s')
    end

    it 'reports the error' do
      expect(subject.error).to eq('AddColumnAuditorToUsers PG::DuplicateColumn: ERROR:  column "auditor" of relation "users" already exists')
    end

    it 'returns true if there is data' do
      expect(subject.any?).to be true
    end

    it 'returns an ASCII table' do
      table = <<~TABLE.rstrip
      
        +--------------------+-----------------------+
        |test ( > 60 seconds)|Duration               |
        +--------------------+-----------------------+
        |Total (all timings) |16s                    |
        |Error               |AddColumnAuditorToUsers|
        +--------------------+-----------------------+
      TABLE

      expect(subject.as_table).to eq(table)
    end
  end
end
