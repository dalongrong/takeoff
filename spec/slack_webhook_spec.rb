# frozen_string_literal: true

require 'spec_helper'
require './lib/slack_webhook'
require './lib/shared_status'
require 'timecop'

describe SlackWebhook do
  subject(:slack) { described_class.new('gstg', '1.0', SharedStatus.new(Time.local(1990))) }

  let(:step) { Steps::ChefStart.new(Roles.new('gstg')) }

  before do
    allow_any_instance_of(SharedStatus).to receive(:username).and_return('login_user')
    allow(step).to receive(:error).and_return('random error')

    allow(slack).to receive(:fire_hook) { |message| message }

    Timecop.freeze(Time.local(1990))
  end

  after do
    Timecop.return
  end

  it 'returns the right deploy started command' do
    expect(slack.deploy_started).to eq('_login_user_ started the deploy of GitLab *v1.0* to *gstg*')
  end

  it 'returns the right deploy resumed command' do
    expect(slack.deploy_resumed).to eq('_login_user_ resumed the deploy of GitLab *v1.0* to *gstg*')
  end

  it 'returns the right warming packages command' do
    expect(slack.warm_up).to eq('_login_user_ is warming up takeoff with GitLab *v1.0* packages :hotsprings:')
  end

  it 'returns the right deploy finished command' do
    expect(slack.deploy_finished).to eq('_login_user_ finished deploying GitLab *v1.0* to *gstg* after 0s :zap: :party-tanuki:')
  end

  it 'returns the right deploy interrupted command' do
    expect(slack.deploy_interrupted(step)).to eq('_login_user_ interrupted the deployment of GitLab *v1.0* to *gstg* on Steps::ChefStart after 0s :bomb-tanuki:')
  end

  it 'returns the right deploy stopped command' do
    expect(slack.deploy_error(step)).to eq('_login_user_ Deployment of GitLab *v1.0* to *gstg* stopped on Steps::ChefStart after 0s :fine:')
  end

  it 'returns the migrations info' do
    expect(slack.migrations('migrations', '1 minute')).to eq('_login_user_ migrations took *1 minute* to run on *gstg*')
  end

  it 'returns the migrations info if there wasn an error' do
    expect(slack.migration_error('migrations', '1 minute', 'an error occurred')).to eq('_login_user_ migrations stopped after *1 minute* on *gstg*')
  end

  it 'returns the qa notification' do
    message = '_login_user_ GitLab *v1.0* has been deployed to *gstg*. Find the QA issue <https://gitlab.com/gitlab-org/release/tasks/issues?label_name[]=QA%20task|here>.'

    expect(slack).to receive(:fire_hook).with(message, channel: '#product')
    expect(slack).to receive(:fire_hook).with(message, channel: '#releases')
    expect(slack).to receive(:fire_hook).with(message, channel: '#development')

    slack.qa_notification
  end
end
